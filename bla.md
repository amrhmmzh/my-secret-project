# Compiling Ardupilot Firmware
Strictly for latest Ardupilot Versions (14/1/22)

## Setup Build Environment in Windows ##

### Windows Subsystem for Linux (WSL) ###

These instructions describes how to setup “Windows Subsystem for Linux” which allows building with waf.
#### Steps ####

1. Enable WSL by opening “Control Panel”, “Programs”, “Turn Windows features on or off” and selecting “Windows Subsystem for Linux”, “Virtual Machine Platform“ and press OK

    <img src="https://gitlab.com/najmehellme0/software-and-control-systems/-/raw/main/Images/turn_on_features.png" align=middle>

2. From a web browser open https://aka.ms/wslstore which should open the Microsoft store to allow installing Ubuntu. Then Launch Ubuntu and fill in a username and password

    <img src="https://gitlab.com/najmehellme0/software-and-control-systems/-/raw/main/Images/install_ubuntu.png"  align=middle>


## Setting Up the WSL Build Environment ##
#### Steps ###

1. Write the following in the command line
    ```sh
    sudo apt-get update
    sudo apt-get install git
    sudo apt-get install gitk git-gui
    ```
2. Clone the Ardupilot Repository
    You can clone the main ardupilot repo or your own fork

    - Main Repo 
    ```sh
    https://github.com/ArduPilot/ardupilot.git 
    ```
    - Fork 
    ```sh
    https://github.com/your-github-account/ardupilot
    ```
3. Clone with command line

    Write the following in the command line
    ```sh
    git clone https://github.com/ArduPilot/ardupilot.git 
    cd ardupilot
    git submodule update --init --recursive
    ```
4. **Important** (Installing Required Packages)

     Write the following in the command line
     ```sh
    cd Tools/environment_install
    ./install-prereqs-ubuntu.sh -y
     ```
5. Go back to ardupilot directory

    Write the following in the command line
    ```sh
    cd ..
    cd ..
    sudo apt-get install arm-none-eabi.ar
    ```

## Basic Command for Building with WAF ##

1. Build ArduCopter

    Below shows how to build ArduCopter for the Pixhawk2/Cube. Many other boards are supported and the next section shows how to get a full list of them.

        ./waf configure --board CubeBlack
        ./waf copter

    For More Detail, head to [link][1]

[1]: https://github.com/ArduPilot/ardupilot/blob/master/BUILD.md
